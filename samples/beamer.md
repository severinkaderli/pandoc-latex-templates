---
title: "Pandoc \\LaTeX\\ Template"
subtitle: "Beamer Example"
author:
    - Severin Kaderli
    - Second Author
---

# Section
## Subsection
This is a subsection.

### Subsubsection
This is a subsubsection.

# Text 
## Formatting

*Italics*

**Bold**

~~Strikethrough~~

# Lists
## Unordered Lists
- First point
- Second point
    - A subpoint
    - Another subpoint
- Third Point

## Ordered Lists
1. First point
2. Second point
    1. A subpoint
    2. Another subpoint
3. Third point


# Math
## Inline
This is an inline math equation: $\sum_{i=0}^{n} \frac{a_i}{1+x}$. The text can
continue afterwards. This is even more text to fill up the remaining space that
the equation takes.

## Block
$$
    x = \sum_{i=0}^{n} \frac{a_i}{1+x}
$$

## SI Units
$$
v = \SI{2.3}{m/s}
$$

# Source Code
## Source Code

```{.python .numberLines}
for i in range(10):
    print("Long test to show that there are automatic line wraps in source code blocks when the line is too long.")
```

# Links
## Hyperlinks
You can easily create links like this [one](https://severinkaderli.ch).
 

## Footnotes
Footnotes are also supported.^[They are very simple to use!]


# Images
## Image

![Placeholder Image](./assets/placeholder.png)

# Blockquotes
## Blockquote

> This is a blockquote.  
> It can span multiple lines!

